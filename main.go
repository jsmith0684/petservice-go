package main

import (
	"github.com/gin-gonic/gin"

	"bitbucket.org/jsmith0684/pet_service/api"
	"bitbucket.org/jsmith0684/pet_service/database"
)

func main() {
	db, _ := database.Initialize()

	port := "3000"
	app := gin.Default() // create gin app
	app.Use(database.Inject(db))
	api.ApplyRoutes(app)
	app.Run(":" + port)
}
