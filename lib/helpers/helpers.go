package helpers

import (
	"crypto/rand"
	"fmt"
)

// GenerateToken generates a random token 32 bytes in length
func GenerateToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
