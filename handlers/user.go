package handlers

import (
	"net/http"

	"bitbucket.org/jsmith0684/pet_service/database/models"
	"github.com/gin-gonic/gin"
)

func GetUser(c *gin.Context) {
	userID := c.Param("id")
	if user, err := models.GetUser(c, userID); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "user not found"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": user})
	}
	return
}

func GetUserPets(c *gin.Context) {
	userID := c.Param("id")
	if user, err := models.GetUser(c, userID); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "user not found"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": user.Pets})
	}
	return
}

func GetUserPetMatches(c *gin.Context) {
	userID := c.Param("id")
	if user, err := models.GetUser(c, userID); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "user not found"})
	} else {
		if petMatches, err := models.GetFilteredPets(c, user); err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "pets not found"})
		} else {
			c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": petMatches})
		}
	}
	return
}

func GetUsers(c *gin.Context) {
	if users, err := models.GetUsers(c); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": err})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": users})
	}
	return
}

func CreateUsers(c *gin.Context) {
	var users models.Users
	if err := c.BindJSON(&users); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": err})
		return
	}
	// if err := users.CreateUsers(c); err != nil {
	if err := models.CreateUsers(c, users); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": err})
	} else {
		c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Users created successfully!", "data": users})
	}
	return
}
