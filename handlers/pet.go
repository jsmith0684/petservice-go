package handlers

import (
	"net/http"

	"bitbucket.org/jsmith0684/pet_service/database/models"
	"github.com/gin-gonic/gin"
)

func GetPets(c *gin.Context) {
	if pets, err := models.GetPets(c); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": err})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": pets})
	}
	return
}

func CreatePets(c *gin.Context) {
	var pets models.Pets
	if err := c.BindJSON(&pets); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": err})
		return
	}
	if err := models.CreatePets(c, pets); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": err})
	} else {
		c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Pets created successfully!", "data": pets})
	}
	return

}
