package apiv1

import (
	"github.com/gin-gonic/gin"

	"bitbucket.org/jsmith0684/pet_service/handlers"
)

// ApplyRoutes applies router to the gin engine
func ApplyRoutes(r *gin.RouterGroup) {
	v1 := r.Group("/v1")
	{
		v1.GET("/pets", handlers.GetPets)
		v1.GET("/ping", handlers.Ping)
		v1.GET("/users", handlers.GetUsers)

		v1.GET("/users/:id", handlers.GetUser)
		v1.GET("/users/:id/pets", handlers.GetUserPets)
		v1.GET("/users/:id/pets/matches", handlers.GetUserPetMatches)

		v1.POST("/pets", handlers.CreatePets)
		v1.POST("/users", handlers.CreateUsers)
	}
}
