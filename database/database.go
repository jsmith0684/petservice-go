package database

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/qor/validations"

	"bitbucket.org/jsmith0684/pet_service/database/models"
)

// Initialize prepares the database for use
func Initialize() (*gorm.DB, error) {
	// db, err := gorm.Open("sqlite3", :memory:)
	db, err := gorm.Open("sqlite3", "database/db.sqlite")
	if err != nil {
		log.Fatalf("could not open sqlite database, %v", err)
	}
	log.Println("Connected to database")
	db.LogMode(true)
	models.Migrate(db)
	validations.RegisterCallbacks(db)
	return db, err
}
