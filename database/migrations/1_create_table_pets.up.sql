CREATE TABLE pets (
  id integer primary key autoincrement not null,
  type text not null,
  name text not null,
  img text,
  sex text check(sex IN ('m', 'f')) not null,
  age integer not null,
  profile text,
  userid integer,
  FOREIGN KEY(userid) REFERENCES users(id)
);