CREATE TABLE settings (
  id integer primary key autoincrement not null,
  profile text,
  typepreference text check(typepreference IN ('cat', 'dog')),
  minage integer,
  maxage integer
);