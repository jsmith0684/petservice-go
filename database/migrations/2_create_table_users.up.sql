CREATE TABLE users (
  id integer primary key autoincrement not null,
  role text not null,
  settingsid integer,
  FOREIGN KEY(settingsid) REFERENCES settings(id)
);