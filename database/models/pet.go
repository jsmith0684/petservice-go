package models

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// Pet structure for pet in database
type Pet struct {
	gorm.Model
	Type    string `json:"type" valid:"in(cat|dog)~type must be cat or dog,required~type is blank"`
	Name    string `json:"name" valid:"required~name is blank"`
	Img     string `json:"img" valid:"-"`
	Sex     string `json:"sex" valid:"in(m|f)~sex must be m or f,required~sex is blank"`
	Age     int    `json:"age" valid:"range(0|20)~age must be between 0 and 20,required~age is blank"`
	Profile string `json:"profile" valid:"required~profile is blank"`
	Users   []User `gorm:"many2many:users_pets;" json:"users,omitempty"`
}

// PetFilters are properties that can limit the number of pet matches
type PetFilters struct {
	Type     string
	Sex      string
	AgeRange struct {
		Min int
		Max int
	}
}

// Pets is a slice containing one or more pet structs
type Pets struct {
	Pets []Pet `json:"pets"`
}

// GetPets retrieves all pets from the database
func GetPets(c *gin.Context) ([]Pet, error) {
	db := c.MustGet("db").(*gorm.DB)
	var pets []Pet
	if err := db.Find(&pets).Error; err != nil {
		return nil, err
	}
	return pets, nil
}

// GetFilteredPets retrieves pets matching filters
func GetFilteredPets(c *gin.Context, user User) ([]Pet, error) {
	db := c.MustGet("db").(*gorm.DB)
	var pets []Pet
	if err := db.Where(&User{TypePreference: user.TypePreference, Sex: user.Sex}).Where("age BETWEEN ? AND ?", user.AgeRange.Min, user.AgeRange.Max).Find(&pets).Error; err != nil {
		return nil, err
	}
	return pets, nil
}

// CreatePets persists a slice of pets to the database
func CreatePets(c *gin.Context, p Pets) error {
	db := c.MustGet("db").(*gorm.DB)
	for _, v := range p.Pets {
		err := db.Create(&v).Error
		if err != nil {
			return err
		}
	}
	return nil
}
