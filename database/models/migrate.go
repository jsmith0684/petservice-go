package models

import (
	"log"

	_ "github.com/golang-migrate/migrate/source/github"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// Migrate runs SQL migrations found in the migrations directory
func Migrate(db *gorm.DB) {
	db.AutoMigrate(&User{}, &Pet{})
	log.Println("Migrations have been processed")
}
