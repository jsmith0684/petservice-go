package models

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"

	"bitbucket.org/jsmith0684/pet_service/lib/helpers"
)

// AgeRange struct embedded in User struct
type AgeRange struct {
	Min int `gorm:"column:min" json:"min" valid:"range(0|20)~min must be in range 0-20"`
	Max int `gorm:"column:max" json:"max" valid:"range(0|20)~max must be in range 0-20"`
}

// User struct for users in database
type User struct {
	gorm.Model
	//ID         int       `json:"id"`
	Profile        string    `json:"profile" valid:"-"`
	Sex            string    `json:"sex" valid:"in(m|f)~sex must be m or f"`
	TypePreference string    `json:"typePreference" valid:"in(cat|dog)~typePreference must be cat or dog"`
	Token          string    `json:"token" valid:"-"`
	Role           string    `json:"role" valid:"in(user|engineer)~role must be user or engineer,required~role is empty"`
	Pets           []Pet     `gorm:"many2many:users_pets;" json:"pets,omitempty"`
	AgeRange       *AgeRange `gorm:"embedded" json:"ageRange"`
}

// Users is a slice containing one or more user structs
type Users struct {
	Users []User `json:"users"`
}

func GetUser(c *gin.Context, userID string) (User, error) {
	db := c.MustGet("db").(*gorm.DB)
	var user User
	id, err := strconv.Atoi(userID)
	if err != nil {
		return user, err
	}
	if err := db.Preload("Pets").First(&user, id).Error; err != nil {
		return user, err
	}
	return user, nil
}

// GetUsers retrieves all users from the database
func GetUsers(c *gin.Context) ([]User, error) {
	db := c.MustGet("db").(*gorm.DB)
	var users []User
	if err := db.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

// CreateUsers persists a slice of users in the database
// func (users Users) CreateUsers(c *gin.Context) error {
func CreateUsers(c *gin.Context, users Users) error {
	db := c.MustGet("db").(*gorm.DB)
	for _, u := range users.Users {
		err := db.Create(&u).Error
		if err != nil {
			return err
		}
	}
	return nil
}

// BeforeSave method generates a new token for a user upon save
func (u *User) BeforeSave() {
	u.Token = helpers.GenerateToken()
}
